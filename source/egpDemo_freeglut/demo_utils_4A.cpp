#include "demo_utils.h"


#include "GL/glew.h"



#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>



//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

// PART 3A: shaders
unsigned int demo::createShaderfromSource(const char *src, unsigned int shaderType)
{
	// validate pointer
	if (src)
	{
		
		// create shader object - needs a handle
		unsigned int handle = glCreateShader(shaderType);
		// If handle is 0/false, shader was never created
		if (handle) { // non-zero = true

			/* Holds the status of the shader, when sent to GPU */
			int status;

			/* Send shader/code to GPU. If glCreateShader works, this *should* as well.
				@param shader's handle from glCreateShader
				@param amount of source files
				@param pointer of source file pointers
				@param status of files (0 for no status)
			*/
			glShaderSource(handle, 1, &src, &status);
		
			/* Compile the shader passed as a handle (from glCreateShader) */
			glCompileShader(handle); 

			/* Check if shader was validated. (Get Shader Integer Value)
				@param the handle from glCreateShader
				@param the type of status you want
				@param a variable to hold the result (passed by reference)
			*/
			glGetShaderiv(handle, GL_COMPILE_STATUS, &status);

			if (status) { // non-zero
				return handle;
			}
		
			// deal with the shader if it was not valid

			int logLength;
			glGetShaderiv(handle, GL_INFO_LOG_LENGTH, &logLength);

			if (logLength > 0) {
				// does something with memory to allocate memory for the log at this specfied location
				char *log = (char *)malloc(logLength);

				/* Gets the actual log from the GPU
					@param the shader handle from glCreateShader
					@param the length of the log
					@param the length of the log (yes twice)
					@param the varaible to store the log in
				*/
				glGetShaderInfoLog(handle, logLength, &logLength, log);

				// Print the log
				printf("\n Shader compile FAILURE: \n%s", log);

				// free the memory alloced by malloc
				free(log);
			}

			// At this point, the shader has failed. We should delete it.
			glDeleteShader(handle);

		}

	}

	// fail
	return 0;
}

void demo::deleteShader(unsigned int shaderHandle)
{
	// validate handle
	if (shaderHandle)
	{
		glDeleteShader(shaderHandle);
	}
}

unsigned int demo::createProgram()
{
	return glCreateProgram();
}

int demo::attachShaderToProgram(unsigned int programHandle, unsigned int shaderHandle)
{
	// validate handles
	if (programHandle && shaderHandle)
	{
		/* Attaches a shader to a program/pipeline
			@param program handle from glCreateProgram
			@param shader handle from glCreateShader
		*/
		glAttachShader(programHandle, shaderHandle);

		return 1;
	}

	// fail
	return 0;
}

int demo::linkProgram(unsigned int programHandle)
{
	// validate handle
	if (programHandle)
	{
		
		/* Links the shaders within a program. Call after glAttachShader
			@param handle from glCreateProgram
		*/
		glLinkProgram(programHandle);

		// check
		int status;

		/* Gets the status from a program
			@param handle from glCreateProgram
			@param status type
			@param variable to hold status
		*/
		glGetProgramiv(programHandle, GL_LINK_STATUS, &status);

		if (status) {
			return programHandle;
		}

		// Log issues

		int logLength;
		glGetProgramiv(programHandle, GL_INFO_LOG_LENGTH, &logLength);

		if (logLength > 0) {
			// does something with memory to allocate memory for the log at this specfied location
			char *log = (char *)malloc(logLength);

			/* Gets the actual log from the GPU
				@param the handle from glCreateProgram
				@param the length of the log
				@param the length of the log (yes twice)
				@param the varaible to store the log in
			*/
			glGetProgramInfoLog(programHandle, logLength, &logLength, log);

			// Print the log
			printf("\n Program %u linking FAILURE: \n%s", programHandle, log);

			// free the memory alloced by malloc
			free(log);
		}

	}

	// fail
	return 0;
}

int demo::validateProgram(unsigned int programHandle)
{
	// validate handle
	if (programHandle)
	{

		/* Validates the shaders within a program. Call after glLinkProgram
			@param handle from glCreateProgram
		*/
		glValidateProgram(programHandle);

		if (getStatusPrintLogProgram(programHandle, GL_VALIDATE_STATUS, "validation")) return programHandle;

	}

	// fail
	return 0;
}

void demo::activateProgram(unsigned int programHandle)
{
	// don't need to validate this one... dual purpose function :)
	glUseProgram(programHandle);
}

void demo::deleteProgram(unsigned int programHandle)
{
	// validate handle
	if (programHandle)
	{
		glDeleteProgram(programHandle);
	}
}

unsigned int demo::getStatusPrintLogProgram(unsigned int handle, GLenum statusType, const char *logMessage) {

	int status;
	
	/* Gets the status from a program
		@param handle from glCreateProgram
		@param status type
		@param variable to hold status
	*/
	glGetProgramiv(handle, GL_VALIDATE_STATUS, &status);

	if (status) return 1;

	int logLength;
	glGetProgramiv(handle, GL_INFO_LOG_LENGTH, &logLength);

	if (logLength > 0) {
		// does something with memory to allocate memory for the log at this specfied location
		char *log = (char *)malloc(logLength);

		/* Gets the actual log from the GPU
		@param the handle from glCreateProgram
		@param the length of the log
		@param the length of the log (yes twice)
		@param the varaible to store the log in
		*/
		glGetProgramInfoLog(handle, logLength, &logLength, log);

		// Print the log
		printf("\n Program %u FAILURE - %s: \n%s", handle, logMessage, log);

		// free the memory alloced by malloc
		free(log);
	}

	return 0;
}

//-----------------------------------------------------------------------------

// file loading
demo::FileInfo demo::loadFile(const char *path)
{
	// default result is null
	FileInfo result = { 0, 0 };

	// validate path
	if (path)
	{
		// open file
		FILE *fp = fopen(path, "rb");
		if (fp)
		{
			// get character count
			fseek(fp, 0, SEEK_END);
			unsigned int newCount = (unsigned)ftell(fp);
			if (newCount)
			{
				// allocate the character string and copy data
				char *newContents = (char *)malloc(newCount + 1);

				// read from beginning and close file
				rewind(fp);
				newCount = fread(newContents, 1, newCount, fp);
				*(newContents + newCount) = 0;
				fclose(fp);

				// release previous data
				result.str = newContents;
				result.len = newCount;
			}
		}
	}

	// exit pass/fail
	return result;
}

int demo::unloadFile(FileInfo *fileInfo)
{
	// check for actual data
	if (fileInfo->str && fileInfo->len)
	{
		free(fileInfo->str);
		fileInfo->str = 0;
		fileInfo->len = 0;

		// done
		return 1;
	}

	// fail
	return 0;
}



//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
