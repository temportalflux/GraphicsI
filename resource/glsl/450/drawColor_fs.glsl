// drawColor_fs.glsl
// fragment shader that receives a colour and applies it to the fragment
#version 450

out vec4 fragColor;

const vec4 orange = vec4(1.0, 0.5, 0.0, 1.0);

void main()
{
	fragColor = orange;
}