// passThru_fs.glsl
// pass-thru fragment shader
#version 450

out vec4 fragColor;

const vec4 orange = vec4(1.0, 0.5, 0.0, 1.0);
const vec4 blue = vec4(0.5, 0.0, 1.0, 1.0);

void main()
{
	fragColor = blue;
}