// passColor_vs.glsl
// vertex shader that passes the incoming color down the pipeline
#version 450

layout (location = 0) in vec4 position;

void main()
{
	gl_Position = position;
}